import { Component, OnInit } from '@angular/core';
import { QuestionsService } from '../../core/services/questions.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { map, reduce, switchMap, tap } from 'rxjs/operators';
import { QuestionModel } from '../../core/state/question.model';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss'],
})
export class PlayComponent implements OnInit {
  gameCompleted$ = this.questionsService.gameCompleted$;
  questions$ = this.questionsService.questions$;
  gettingQuestions$ = this.questionsService.gettingQuestions$;
  getQuestionsSubscription: Subscription = this.route.queryParams
    .pipe(
      switchMap((params) =>
        this.questionsService.getQuestions({
          type: params.type,
          amount: params.amount,
          difficulty: params.difficulty,
        })
      )
    )
    .subscribe();

  constructor(
    private readonly route: ActivatedRoute,
    private readonly questionsService: QuestionsService
  ) {}

  ngOnInit(): void {}

  onAnswerClicked(
    questionId: QuestionModel['_id'],
    answerSelected: string
  ): void {
    this.questionsService.selectAnswer(questionId, answerSelected);
  }

  checkAnswers(): void {
    this.questionsService.completeGame();
  }

  calculateCorrectAnswers(): void {
    // this.questions$
    //   .pipe(
    //     map((x) =>
    //       x.map((y) => {
    //         const rightAnswer = y.answers.find((z) => z.isCorrect);
    //         const correctAnswer = rightAnswer?._id === y.selectedId;

    //         return correctAnswer ? 1 : 0;
    //       })
    //     ),
    //     map(x => x.reduce((prev, next) => +prev + +next, 0))
    //   )
    //   .subscribe();
  }
}
